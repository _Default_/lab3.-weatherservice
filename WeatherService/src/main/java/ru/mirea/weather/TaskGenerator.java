package ru.mirea.weather;

import ru.mirea.DataSourceAPI;
import ru.mirea.DataSourceImpl;
import ru.mirea.weather.DataQueue;
import ru.mirea.weather.Task;

import java.util.Set;

public class TaskGenerator implements Runnable {
    private DataQueue inQueue;
    private int Num = 0;


    public TaskGenerator(DataQueue inQueue)
    {
        this.inQueue = inQueue;
    }
    private DataSourceAPI ds = new DataSourceImpl();

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                for (String city : ds.cities()) {
                    inQueue.push(new Task(Num, city));
                    Num++;
                    Thread.sleep(10);
                }
            }

        } catch (InterruptedException e){
            //     e.printStackTrace();
        }
    }
}
