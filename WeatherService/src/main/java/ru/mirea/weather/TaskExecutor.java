package ru.mirea.weather;

import ru.mirea.DataSourceAPI;
import ru.mirea.DataSourceImpl;


public class TaskExecutor implements Runnable{
    private DataQueue inQueue, outQueue;
    private DataSourceAPI ds = new DataSourceImpl();

    public TaskExecutor(DataQueue inQueue, DataQueue outQueue) {
        this.inQueue = inQueue;
        this.outQueue = outQueue;
    }
    @Override
    public void run() {
        try {
            while(!Thread.interrupted()){
                Task task = inQueue.pop();
                if (task != null) {
                    task.weather = ds.getWeather(task.city);
                    outQueue.push(task);
                }
            } Thread.sleep(10);
        }catch (InterruptedException e)
        {
        }


    }
}
