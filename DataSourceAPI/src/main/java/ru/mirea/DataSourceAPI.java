package ru.mirea;

import java.util.Set;

public interface DataSourceAPI {
    Set<String> cities();
    String getWeather(String city);
}
