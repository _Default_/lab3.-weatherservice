package ru.mirea;

import org.junit.Test;

import static org.junit.Assert.*;

public class DataSourceImplTest {

    DataSourceAPI ds = new DataSourceImpl();

    @Test
    public void cities() {
        assertEquals(true, ds.cities().contains("Москва"));
        assertEquals(4, ds.cities().size());
    }

    @Test
    public void getWeather() {
        assertEquals("+30",ds.getWeather("Барселона"));
    }
}
