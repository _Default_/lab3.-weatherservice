package ru.mirea;
import ru.mirea.DataSourceAPI;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DataSourceImpl implements DataSourceAPI {
    private Map<String,String> cities = new HashMap <String, String>();
    public DataSourceImpl(){
        cities.put("Москва", "+20");
        cities.put("Санкт-Петербург", "+12");
        cities.put("Барселона", "+30");
        cities.put("Лондон", "+17");
    }
    public Set<String> cities(){
        return this.cities.keySet();
    }

    public String getWeather(String city){
        return this.cities.get(city);
    }
}
